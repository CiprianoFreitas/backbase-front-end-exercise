# Backbase Weather Exercise #
## Frontend Developer Exercise 2.2 by Cipriano Freitas##

### You can access the app here: http://backbase-weather.surge.sh/ ###

[![weatherapp.gif](https://gifyu.com/images/weatherapp.gif)](https://gifyu.com/image/bCLR)

This application shows the weather for different cities, it can also show an hourly forecast and a graph.
The app was developed using Angular 1.6 and bundled with webpack, for the UI the [AngularJS Material](https://material.angularjs.org/latest/) library was used.

The cities list comes from a cloud database (https://airtable.com/shr6VchQGdZxc4K0y) which provides a REST api. You can edit the table by accessing [this link](https://airtable.com/invite/l?inviteId=invZ3N18SyxK1sZJD&inviteToken=4b38ccdfecc7a098fac6edb1008c2a6c) with an Airtable account.

This application was developed with future proof code. With Angular2 (and Angular4) already release there are a few best-practices that the Angular team recommends for us to make the upgrade from Angular1 to Angular 2 easier:

* **Using components instead of directives**

    With Angular 1.5 we're able to create components that are similar to Angular2 components which are based in the web components standard.

* **Avoid using $scope**

    $scope and $rootScope are a source of confusion and creates a code mess in a lot of angular projects, it's hard to know which $scope we're accessing when we have directives inside of other directives. Angular2 got rid of $scope so we don't use it in this app preferring the controllerAs approach.

### How to run the app locally ###
* run **yarn** or **npm install** to install the dependencies
* run **yarn bundle** or **npm bundle**
* run **yarn run dev** or **npm run dev** to start a local server

### Things I would improve ###
* **Error handling** - This app is very optimistic, it trust that all of the requests are successful and that they have all of the data we need.
* **Animation performance** - The slide-down animation of the hourly forecast seems to stutter on my phone, I think it's because I'm making a request to openweathermap at the same same and assigning some variables with the result and angular sometimes lags when assigning variables because of the $digest cycle.
* **Code Splitting** - Instead of creating just one huge bundle with all of the apps logic I would've preferred to use webpack's code splitting feature to lazy load parts of the app as they are needed (the graph section is a good example as it may not be loaded at all in some cases).
* **Sourcemaps** - I usually generate sourcemaps for all of my minified files or when I'm working with typescript, in this case it wasn't a high priority.
* **Testing** - I usually have some kind of E2E testing, although I usually use generators for that which I didn't do this time.