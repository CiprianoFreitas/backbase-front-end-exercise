var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    context: __dirname + '/app',
    entry: {
        app: './app.js',
        vendor: [
            'angular',
            'angular-material',
            'angular-resource',
            'moment',
            'angular-moment',
            'chart.js',
            'angular-chart.js'
        ]
    },
    output: {
        path: __dirname + '/assets',
        filename: 'app.bundle.js'
    },
    module: {
        rules: [{
            test: /\.html$/,
            loader: 'ngtemplate-loader!html-loader'
        },
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        },
        {
            test: /\.css$/,
            loader: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: {
                    loader: 'css-loader', options: { minimize: true }
                }
            })
        }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            filename: "vendor.bundle.js"
        }),
        new ExtractTextPlugin("[name].bundle.css"),
        new webpack.optimize.UglifyJsPlugin({ minimize: true })
    ]
};