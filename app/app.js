angular.module('app', [
    'ngMaterial',
    'ngResource',
    'angularMoment',
    'chart.js'
]);

//import styles
require('angular-material/angular-material.css');
require('./style/style.css');

require('./services');
require('./components');