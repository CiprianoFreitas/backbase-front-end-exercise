'use strict';
require('./style.css');
var templateUrl = require('./forecast.html');

ForecastController.$inject = ['weatherService'];

function ForecastController(weatherService) {
    const numForecasts = 4;
    this.forecastItems = [];

    this.getSimplifiedForecast = (data) => {
        var forecastItems = data.list.slice(0, numForecasts);
        return forecastItems.map((item) => { return { 'date': item.dt, 'temp': item.main.temp, 'windSpeed': item.wind.speed } });
    };

    this.$onInit = () => {
        weatherService.getHourlyForecast(this.locationName)
            .then((data) => {
                this.forecastItems = this.getSimplifiedForecast(data);
            });
    };

}

const forecastComponent = {
    templateUrl: templateUrl,
    bindings: {
        locationName: '='
    },
    controller: ForecastController
};

module.exports = forecastComponent;