'use strict';

var angular = require('angular');

angular.module('app')
    .component('appContainer', require('./container/container.component'))
    .component('locationsList', require('./locations-list/locations-list.component'))
    .component('locationContent', require('./location-content/location-content.component'))
    .component('graph', require('./graph/graph.component'))
    .component('forecast', require('./forecast/forecast.component'));