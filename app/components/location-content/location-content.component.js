'use strict';

require('./style.css');
var templateUrl = require('./location-content.html');

LocationContentController.$inject = ['weatherService'];

function LocationContentController(weatherService) {
    this.$onInit = () => {
        weatherService.getLocationData(this.locationName)
            .then((data) => {
                this.temp = data.main.temp;
                this.wind = data.wind.speed;
                this.weatherIcon = data.weather[0].icon;
            });
    };

    this.toggleWeekForecast = () => {
        this.showWeekForecast = !this.showWeekForecast;
    };
}

const locationContentComponent = {
    templateUrl: templateUrl,
    bindings: {
        locationName: '=',
        isLastItem: '='
    },
    controller: LocationContentController
};

module.exports = locationContentComponent;