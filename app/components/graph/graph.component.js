'use strict';

var templateUrl = require('./graph.html');
GraphController.$inject = ['moment']
function GraphController(moment) {
    this.$onInit = () => {
        this.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }
                ]
            }
        };

    };

    this.$onChanges = function (changes) {
        this.labels = changes.forecast.currentValue.map(item => moment.unix(item.date).format('LT'));
        this.data = changes.forecast.currentValue.map(item => item.temp);
    };
}

const graphComponent = {
    templateUrl: templateUrl,
    bindings: {
        forecast: '<'
    },
    controller: GraphController
};

module.exports = graphComponent;