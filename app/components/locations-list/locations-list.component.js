'use strict';
var templateUrl = require('./locations-list.html');

LocationsListController.$inject = ['locationsService'];

function LocationsListController(locationsService) {
    this.$onInit = function () {
        var _this = this;
        //initialize empty array with all of the locations
        this.locations = [];
        locationsService.getLocations()
            .then((locations) => {
                _this.locations = locations;
            });
    };

}


const locationsListComponent = {
    templateUrl: templateUrl,
    controller: LocationsListController
};

module.exports = locationsListComponent;