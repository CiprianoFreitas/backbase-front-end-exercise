'use strict';
var templateUrl = require('./container.html');

const ContainerController = () => {
};

const containerComponent = {
  templateUrl: templateUrl,
  controller: ContainerController
};

module.exports = containerComponent;
