'use strict';

LocationsService.$inject = ['$http'];

//Service used to fetch the locations from the cloud database
function LocationsService($http) {
    const apiUrl = 'https://api.airtable.com/v0/applhkAsQTwRAMmcJ/Locations';

    this.getLocations = () => {
        return $http({
                method: 'GET',
                url: apiUrl,
                headers: {
                    'Authorization': 'Bearer keyEbt30FI4fB486D'
                }
            })
            .then((res) => {
                return res.data.records.map((record)=> record.fields.Name);
            });

    };
}

module.exports = LocationsService;