'use strict';

WeatherService.$inject = ['$http'];

//Service use to get openweathermap data
function WeatherService($http) {
    var currentWeatherApiUrl = 'http://api.openweathermap.org/data/2.5/weather?units=metric&q=';
    var forecastApiUrl = 'http://api.openweathermap.org/data/2.5/forecast?units=metric&q=';
    var apiId = 'appid=3d8b309701a13f65b660fa2c64cdc517';

    this.getLocationData = (location) => {
        const reqUrl = `${currentWeatherApiUrl}${location}&${apiId}`;
        return $http.get(reqUrl)
            .then(res => {
                return res.data;
            });
    };

    this.getHourlyForecast = (location) => {
        const reqUrl = `${forecastApiUrl}${location}&${apiId}`;
        return $http.get(reqUrl)
            .then(res => {
                return res.data;
            });
    };
}

module.exports = WeatherService;