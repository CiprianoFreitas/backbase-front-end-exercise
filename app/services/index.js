'use strict';

var angular = require('angular');

angular.module('app')
    .service('locationsService', require('./locations.service'))
    .service('weatherService', require('./weather.service'));